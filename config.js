module.exports = {

  // 開発時 = true , 公開時 = false
  develop: false,


  // 公開ディレクトリ、開発ディレクトリの設定
  directory: {
    source: './src',
    public: './public_html'
  },


  // CSSの設定
  css: {
    style: './src/assets/css/**/!(_)*.scss',
    vendor: [
      './node_modules/reset-css/reset.css',
      './node_modules/slick-carousel/slick/slick.css',
    ],
    browsers: ['last 4 versions', 'ie 9', 'ie 10', 'android 4'],
    output: './public_html/assets/css/',
  },


  // JSの設定
  js: {
    script: './src/assets/js/script.js',
    vendor: './src/assets/js/vendor.js',
    output: __dirname + '/public_html/assets/js'
  },


  // 削除ファイルリスト（公開時などに削除したいファイルを追加してください）
  del: {
    list :['**/*.map']
  }

}